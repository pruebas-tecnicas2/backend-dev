package com.zara.backenddev.service;

import com.zara.backenddev.client.ProductClient;
import com.zara.backenddev.dto.ProductDTO;
import com.zara.backenddev.exception.ErrorCatalog;
import com.zara.backenddev.exception.NotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService{

	private final ProductClient productClient;

	@Override
	public ProductDTO getProductById(String id) {

		return this.productClient.getProductById(id)
			.orElseThrow(() -> new NotFoundException("Product not found", ErrorCatalog.PRODUCT_NOT_FOUND));
	}

	@Override
	public List<ProductDTO> getSimilarProducts(String id) {

		return productClient.getSimilarProductId(id).stream().map(this::getProductById).collect(Collectors.toList());
	}
}
