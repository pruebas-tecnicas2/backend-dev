package com.zara.backenddev.service;

import com.zara.backenddev.dto.ProductDTO;
import java.util.List;

public interface ProductService {

	ProductDTO getProductById(final String id);

	List<ProductDTO> getSimilarProducts(final String id);
}
