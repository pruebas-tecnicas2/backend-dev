package com.zara.backenddev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableFeignClients
@EnableConfigurationProperties
@ImportAutoConfiguration({FeignAutoConfiguration.class})
public class BackendDevApplication {

	public static void main(String[] args) {

		SpringApplication.run(BackendDevApplication.class, args);
	}

}
