package com.zara.backenddev.client;

public final class ClientConstants {

	public static final String PRODUCT_ID_PATH = "/product/{productId}";
	public static final String SIMILAR_PRODUCT_ID_PATH = "/product/{productId}/similarids";
}
