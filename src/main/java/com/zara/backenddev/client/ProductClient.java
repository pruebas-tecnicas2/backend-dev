package com.zara.backenddev.client;

import static com.zara.backenddev.client.ClientConstants.PRODUCT_ID_PATH;
import static com.zara.backenddev.client.ClientConstants.SIMILAR_PRODUCT_ID_PATH;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.zara.backenddev.dto.ProductDTO;
import feign.FeignException;
import java.util.List;
import java.util.Optional;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "similar-product-client", url = "${client.product.url}", fallback = FeignException.class)
public interface ProductClient {

	String PRODUCT_ID = "productId";

	@RequestMapping(value=PRODUCT_ID_PATH, method= RequestMethod.GET)
	Optional<ProductDTO> getProductById(@PathVariable (name= PRODUCT_ID) String id);
	@GetMapping(value = SIMILAR_PRODUCT_ID_PATH, produces = APPLICATION_JSON_VALUE)
	List<String> getSimilarProductId(@PathVariable(name = PRODUCT_ID) String promotionId);
}
