package com.zara.backenddev.controller;

import com.zara.backenddev.dto.ProductDTO;
import com.zara.backenddev.service.ProductService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ProductController {

	public static final String PRODUCT_ID = "productId";

	private final ProductService productService;

	@GetMapping("/product/{productId}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<ProductDTO> product(@PathVariable(PRODUCT_ID) final String id) {

		return ResponseEntity.ok(this.productService.getProductById(id));
	}

	@GetMapping("/product/{productId}/similarids")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<ProductDTO>> similarProducts(@PathVariable(PRODUCT_ID) final String id) {

		return ResponseEntity.ok(this.productService.getSimilarProducts(id));
	}
}
