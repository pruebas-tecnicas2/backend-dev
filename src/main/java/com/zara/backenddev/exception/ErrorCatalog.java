package com.zara.backenddev.exception;

public enum ErrorCatalog {

	PRODUCT_NOT_FOUND("PRODUCT-ID-01");
	private final String code;

	ErrorCatalog(final String code) {

		this.code = code;
	}

	public String getCode() {

		return code;
	}
}
