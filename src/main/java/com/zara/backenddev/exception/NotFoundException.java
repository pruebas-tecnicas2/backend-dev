package com.zara.backenddev.exception;

public class NotFoundException extends RuntimeException{

	private final ErrorCatalog errorCatalog;

	public NotFoundException(final String message, final ErrorCatalog errorCatalog){
		super(message);
		this.errorCatalog = errorCatalog;
	}

	public ErrorCatalog getErrorCatalog(){
		return this.errorCatalog;
	}

	public static NotFoundException create(final String message, final ErrorCatalog errorCatalog){
		return new NotFoundException(message, errorCatalog);
	}

}
