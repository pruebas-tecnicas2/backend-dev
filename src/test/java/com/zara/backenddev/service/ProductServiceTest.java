package com.zara.backenddev.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.zara.backenddev.client.ProductClient;
import com.zara.backenddev.dto.ProductDTO;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {


	private static final String PRODUCT_ID = "1";
	@InjectMocks
	ProductServiceImpl productServiceImpl;

	@Mock
	ProductClient productClient;

	@BeforeEach
	void Init() {

		productServiceImpl = new ProductServiceImpl(this.productClient);
	}

	@Test
	void should_return_prodcut() {

		final ProductDTO productDTO = ProductDTO.builder().id(PRODUCT_ID).build();

		when(this.productClient.getProductById(any())).thenReturn(Optional.of(productDTO));

		ProductDTO productResponseDTO = this.productServiceImpl.getProductById(PRODUCT_ID);

		assertEquals("1", productResponseDTO.getId());
	}
}
