package com.zara.backenddev.controller;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anEmptyMap;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.OK;

import com.jayway.jsonpath.JsonPath;
import com.zara.backenddev.dto.ProductDTO;
import com.zara.backenddev.service.ProductService;
import io.restassured.http.ContentType;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class ProductControllerTest {

	@InjectMocks
	ProductController productController;

	@Mock
	ProductService productService;

	protected static final String PRODUCT_ID = "1";

	@Test
	void should_get_product() {

		when(this.productService.getProductById(any())).thenReturn(
			ProductDTO.builder().build());

		final ResponseEntity<ProductDTO> response =
			this.productController.product(PRODUCT_ID);

		assertEquals(OK, response.getStatusCode());

	}

}
